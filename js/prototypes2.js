const Vehicle = function (name, type) {
  this.name = name;
  this.type = type;
};
Vehicle.prototype.makeSound = function () {
  console.log("make some sound");
};

const Car = function (carName, carType, brand) {
  Vehicle.call(this, carName, carType);
  this.brand = brand;
};
Car.prototype = Object.create(Vehicle.prototype);
Car.prototype.makeSound = function(){
    console.log('make smooth sound')
}
const car1 = new Car('creta', 'suv', 'hyundai');
car1.makeSound();
console.log(car1.name);
console.log(car1.type);
console.log(car1.brand)

class Employee {
    constructor(id, name, salary){
        this.id = id;
        this.name = name;
        this.salary = salary;
    }
    getIntro() {
        console.log(`I am ${this.name} and I earn ${this.salary}`)
    }
}

class SoftwareEnginner extends Employee{
    constructor(id,name,salary){
        super(id,name,salary)
    }
    // getIntro(){
    //     console.log(`I am a software engineer`)
    // }
}

const softwareEnginner = new SoftwareEnginner(1,'xyz',2000)
softwareEnginner.getIntro()

//Data hiding example with closures.
const Counter = function () {
  this.count = 0;
  this.increment = function () {
    this.count += 1;
    console.log(this.count);
  };
  this.decrement = function () {
    this.count -= 1;
    console.log(this.count);
  };
};

const counter1 = new Counter();
counter1.increment();
counter1.increment();
counter1.increment();
counter1.decrement();


