// console.log("hello world");
// let Myc = function () {
//   this.a = "hello";
//   this.b = "world";
// };

// Myc.prototype.a = "john";
// Myc.prototype.c = 12;

// let obj = new Myc();
// console.log(obj.c);
// console.log(obj.a);
// console.log(obj.b);
// // console.log(obj.d);

// var c = function (n) {
//   this.n = n;
// };

// c.square = function () {
//   return this.n * this.n;
// };

// // var obj = new c(4);

// // console.log(obj.square());

// function foo(a = 10, b = 7, c) {
//   console.log(a, b, c);
//   for (i = 0; i < 5; i++) {
//     console.log(i);
//   }
//   return a + b + c;
// }

// console.log(foo(23));
// console.log(i);

// function foo1(a, b) {
//   console.log(a, b);
// }

// foo1(23);

// let str = "asdcmnasdffsfasdasdfff";
// let objc = str.split("").reduce((acc, cur) => {
//   if (acc[cur]) {
//     acc[cur]++;
//   } else {
//     acc[cur] = 1;
//   }
//   return acc;
// }, {});
// console.log(objc);

//Custom Debounce Demo
document.onreadystatechange = () => {
  if (document.readyState === "complete") {
    let input = document.getElementById("input");
    let x = document.getElementsByClassName("in");
    input.addEventListener("keyup", (e) => {
      debouncedFetch(e, "hello world");
    });
    console.log(input, x);
  }
};

const debounce = (fn) => {
  let timer;
  return (e) => {
    if (timer) {
      clearTimeout(timer);
    }
    timer = setTimeout(() => {
      fn();
      console.log(e.target.value);
    }, 300);
  };
};

const fetchData = () => {
  console.log("fetching ...");
};

const debouncedFetch = debounce(fetchData);

function sumDigProd(...params) {
	if(params.length === 1 && params[0] <10) return params[0];
	let sum = params.reduce((acc,cur)=>acc+cur,0);
  console.log(sum)
	let result = [];
	let temp = 0;
	while(sum!=0){
		let digit = sum % 10
		sum = Number.parseInt(sum/10) 
		result.push(digit);
    console.log(result)
		if(sum === 0){
      console.log(result)
			temp = result.reduce((acc,cur)=>acc*cur,1);
			if(Number.parseInt(temp / 10)   !== 0){
				sum = temp;
				result.length = 0;
			}
			else{
				return temp;
			}
		}
	}
}
console.log(sumDigProd(8, 16, 89, 3))

sumDigProd(16, 28)
sumDigProd(9)
sumDigProd(26, 497, 62, 841)
sumDigProd(0)
document.onreadystatechange = () => {
  if (document.readyState === "complete") {
    const ulElement = document.getElementsByTagName("ul"); //returns htmlcollection
    console.log(ulElement.item(0));

    const liElement = document.getElementsByTagName("li");
    console.log(liElement.namedItem("list")); //returns first found named item either by ID or Name

    const classes = document.getElementsByClassName("myclass");
    console.log(classes);

    const names = document.getElementsByName("list"); //returns nodelist
    console.log(names);
  }
};
