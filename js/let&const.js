//TDZ, let-const, shadowing, block scope

// console.log(a);
//TDZ
let a = 35;
// let a = 233; //syntax error
var b = 656;
var b = 99; //not allowed in case of let and const
{
  //Block scope works for let and const.
  //var has function scope.
  //illegal shadowing is done if var is used to shadow a let in block scope.
  //   var a = 323; //illegal shadow. var crossed boundary and gets hoisted at top in global object
  this.myFunction();
  myFunction();
  let b = 2300;
  var myVar = 989;
  function myFunction() {
    //block scoped and undefined in global scope.
    console.log("logging my function -- INSIDE");
  }
  var arrowFunc = () => {
    //block scoped only.
    console.log("logging my arrowFunc");
  };
}
console.log(b);
const x = "hello";
// x = 43;   //type error
function myFunction() {
  console.log("logging my function -- OUTSIDE");
}
