///Lexical environment, scope chains, Execution contexts in browser, call stack.

var x = 1;

function a() {
  console.log("logging from a");
  console.log(x);
  var myVar = 23;
  // this.b();  //this is window object and contains the definition of outside b in global scope.
  b();
  function b() {
    console.log("logging from b");
    console.log(x);
    console.log(myVar);
  }
}
//example for lexical environment

//b is lexically present outside in global scope.
//so, b does not have access to local variable of a i.e myVar
//If b was lexically inside a, then it would know myVar due to scope chaining.

function b() {
  console.log("logging from b -- OUTSIDE");
  console.log(x);
  console.log(myVar);
}

a();
