// Closures

function z() {
  var b = 4823;
  function x() {
    var a = 123;
    function y() {
      console.log(a, b);
      // console.log('inside y',this)
    }
    a = 99;
    b = 101;
    // console.log('inside x',this)
    return y;
  }
  // console.log('inside z',this)
  return x();
}
// z();
var w = z(); //closure x and closure z is returned along with the function y
//w contains the lexical scope of y and function y itself
//As the lexical scope is remembered, the references to a and b are known and any updated values are also reflected in closures.
w();
console.log(w);

function trick() {
  for (var i = 0; i < 5; i++) {
    //as let is block scoped, new binding is passed to setTimeout.
    (function myHack(x) {
      //in this case newwcopy if i is created thus new reference is present with setTimeout everytime it is called.
      setTimeout(() => {
        console.log(x);
      }, x * 1000);
    })(i);
  }
}

// trick();

function outer() {
  var x = 983;
  function inner() {
    console.log(x, y);
  }
  let y = 32;
  var z = 98; //smartly garbage collected as it is not used in the inner function.
  return inner;
}
outer()();

function myMethod() {
  setTimeout(() => {
    console.log(this.name); //if function is used then this points to global object
  }, 2000);
}

//arrow functions could not be bound to contexts using call, bind apply as they are lexically bound to parent
// const myAsyncMethod = ()=> {
//   setTimeout(() => {
//     console.log(this.name); //if function is used then this points to global object
//   }, 2000);
// } 

function myMethod2() {
  let x =  ()=> {
    console.log(this);
    console.log(this.name);
    setTimeout(() => {
      console.log(this.name); //if function is used then this points to global object
    }, 2000);
  };
  x.call(this);
}
let obj = {
  name: "mayuresh",
};
// myMethod.call(obj);
myMethod2.call(obj);
