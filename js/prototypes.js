let x = {
  name: "Gaitonde",
  address: "Mumbai",
};
console.log(x.prototype); //does not contain prototype instead use __proto__
console.log(x.__proto__);
console.log(typeof x);

let y = function (name, address) {
  this.name = name;
  this.address = address;
  this.getIntro = function () {
    console.log(this.name + " is from " + this.address);
  };
};

//Now logging Y
console.log("---------------------Now logging Y------------------------");
console.log(y.prototype); //prototype of function itself
console.log(typeof y); //function
console.log(y.__proto__.__proto__); //prototype of base master Object
console.log(y.__proto__);

let person1 = new y("Ganpat", "Pune");
let person2 = new y("Shanpat", "Pune");
console.log(person1);
console.log(person2);
person1.getIntro();

console.log("-------------------Equality and type checks--------------");
let obj1 = {
  name: "raja",
};

let obj2 = {
  name: "raja",
};

console.log(true == 1);
console.log(obj1 == obj2);
console.log(0 == 0n);
console.log(0 === 0n);
console.log(0 === -0);
console.log(0n == -0);

let myObj = {
  fun: function () {
    console.log(this);
  },
};

let myFunc = myObj.fun;
myFunc();

myObj.fun();
