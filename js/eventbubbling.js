const useCapturing = true;

document.onreadystatechange = function () {
  if (document.readyState === "complete") {
    let element = document.getElementById("grandparent");
    element.addEventListener(
      "click",
      (e) => {
        console.log("grandparent clicked");
        e.stopPropagation();
      },
      !useCapturing
    );
    document.getElementById("parent").addEventListener(
      "click",
      (e) => {
        console.log("parent clicked");
        e.stopPropagation();
      },
      !useCapturing
    );
    document.getElementById("child").addEventListener(
      "click",
      () => {
        console.log("child clicked");
      },
      { useCapturing: !useCapturing, once: true }
    );
  }
};
