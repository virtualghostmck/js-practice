//1.---------------
let arr1 = [2,4,6]
let arr2 = [7,3,7]
console.log([arr1 + arr2]); //["2,4,67,3,7"]
console.log(arr1 + arr2); //2,4,67,3,7
console.log([...arr1, ...arr2]);
console.log(arr1.concat(arr2))

let arr3 = []
arr3[51] = 123;
delete arr3[51]
console.log(arr3)
arr3.fill(0)
console.log(arr3)
console.log(arr3.length)

//2.----------------- Template literals
function foo(strings,param1, param2) {
    let x = strings[0], y = param1,w=strings[1], z = param2;
    return x + y + w+z;
}
console.log(foo`hello ${`world`} another string ${`other param`}`)


//3.Scope of this and context of arguments.
function y() {
    console.log('length is ', this.length, this)
}

let myObject = {
    length : 10,
    y2 : ()=> {
        console.log('inside object length is ', this.length,this)
    },
    myFoo : function(callback){
        arguments[0]() //function called using context of arguments
    },
    myFoo2: function(callback){
        callback()
    },
    myFoo3: function(callback){
        callback.call(this)
    }
}
myObject.myFoo(y,2,3)
myObject.myFoo(myObject.y2,2,3)
myObject.myFoo2(y,2,3)
myObject.myFoo2(myObject.y2,2,3)
myObject.myFoo3(y,2,3)
myObject.myFoo3(myObject.y2,2,3)

//4.string constructor ----------------------
let myStr = 'constructor'
console.log(myStr[myStr](011)) //base 8 number to decimal value 9

//5. -------------------
let myFoo = function () {
    // console.log(arguments)
    return arguments.length
}

console.log(myFoo(1,2,3,()=>{},7,()=>{}))

//6.---------------
let A = {
    x: function(){
        console.log('x')
        return this
    },
    y: function(){
        console.log('y')
        return this 
    },
    z: function(){
        console.log('z')    
    }

}

A.x().y().z()

//7.---------------
const userInfo = {
    name:'xyz',
    location:{
        city:'pune'
    }
}

// const user_copy = {...userInfo}

// console.log('original',userInfo)
// user_copy.name='pqr'
// user_copy.location.city = 'delhi'

// console.log('original',userInfo)
// console.log('copy',user_copy)
//----------------------------------------------
//Deep clone solution, also we can use cloneDeep lodash function

// const user_copy = JSON.parse(JSON.stringify(userInfo)) 
// console.log('original',userInfo)
// user_copy.name='pqr'
// user_copy.location.city = 'delhi'

// console.log('original',userInfo)
// console.log('copy',user_copy)

//-------------------------------
// Clone by deep spreading objects.

// const user_copy = {...userInfo, name:'pqr', location:{...userInfo.location, city:'delhi'}}
// console.log('original',userInfo)
// console.log('original',userInfo)
// console.log('copy',user_copy)


const user_copy = {...userInfo, location:{...userInfo.location}}
console.log('original',userInfo)
user_copy.name='pqr'
user_copy.location.city = 'delhi'
console.log('original',userInfo)
console.log('copy',user_copy)


//Common elements in two arrays:
let arrr1 = [1,2,3,4,5,6,7,8,9,2,4,6]
let arrr2 = [1,2,3,5,11,9,8,23]

if (arr1.length >= arr2.length) {
    let res = arrr1.reduce((acc,cur) =>{
        if (arrr2.includes(cur)) {
            acc.push(cur)
        }
        return acc
    },[])   
    console.log(res)
    //---remove duplicate elements
    res = Array.from(new Set(res))
    console.log(res)
}
