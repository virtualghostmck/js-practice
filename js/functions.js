let b = function xyz() {
  //named funciton expression.
  console.log("hello");
  return xyz; //xyz accessible inside blocks but not in global scope
};

b()();

const x = function () {
  let z = "asf";
  return function () {
    console.log(z);
  };
};

x()(); //first class function example.

function myAsync() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({ key: 123, value: "this is my async result" });
    }, 2000);
  });
}

function nonAsync() {
  myAsync().then((res) => console.log(res));
}

// nonAsync();

const anotherAsync = async () => {
  return await myAsync();
};

const callAsync = async () => {
  return await anotherAsync();
};

callAsync().then((res) => {
  let a = 2;
  if (a === 2) {
    console.log(res);
  }
});

const xyz = async () => {
  return Promise.resolve("hello");
};
const dbc = async () => {
  return "promise string";
};

console.log(xyz());
xyz().then((res) => console.log(res));

console.log(dbc());
dbc().then((r) => console.log(r));
// function myPromise() {
//   return new Promise((resolve, reject) => {
//     setTimeout(() => {
//       resolve({ key: 123, value: "this is my async result" });
//     }, 2000);
//   });
// }

// myPromise().then((res) => console.log(res));

console.log(Promise.prototype);

let myTestObj = {
  key: "abc",
  normalFunc: function(){
    console.log(this);
    console.log(this.key);
  },
  arrowFunc: () => {
    console.log(this);
    console.log(this.key);
  },
};

myTestObj.arrowFunc();
myTestObj.normalFunc();
