const myObj = {
  name: "Hemangini",
  address: "Kondhwa",
};

const myAddress = function(petName, breed){
  console.log(
    `I am ${this.name} and I live in ${this.address}. ${petName} is my pet and he is a ${breed}`
  );
};

let foo = myAddress.bind(myObj, 'pluto', 'pug');
foo();

 //--------------------------------------------------------

Function.prototype.myBindPolyFill = function (ref, ...params) {
  let myFunc = this;
  return function (...args) {
    myFunc.apply(ref, [...params, ...args]);
  };
};
let x = myAddress.myBindPolyFill(myObj, "Pluto");
x("Pug");

// myAddress.call(myObj);
